package bg.androidapps.birthdaysremindme;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class HelpActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		
		setTitle(R.string.helpActivityTitle);
		
		((TextView)findViewById(R.id.help)).setText(Html.fromHtml(getString(R.string.help1) + " " + getString(R.string.help2) + "<br><br>"
				+ getString(R.string.help3) + "<br><br>"
				+ getString(R.string.help4) + "<br><br>"
				+ getString(R.string.help5) + "<br><br>"
				+ getString(R.string.help6) + "<br><br>"
				+ getString(R.string.help7) + "<br><br>"
				+ getString(R.string.help8) + "<br>"
				+ getString(R.string.help9) + "<br>"
				+ getString(R.string.help10) + "<br>"
				+ getString(R.string.help11) + "<br>"
				+ getString(R.string.help12) + "<br><br>"
				+ getString(R.string.help14) + "<br><br>"
				+ getString(R.string.help15) + "<br><br>"
				+ getString(R.string.help16) + "<br><br>"
				+ getString(R.string.help17) + "<br><br>"
				+ getString(R.string.help13)
				+ " <b>kplamen@gmail.com</b>."));
	}
}
