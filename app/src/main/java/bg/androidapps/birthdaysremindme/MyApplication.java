package bg.androidapps.birthdaysremindme;

import java.util.Locale;

import bg.androidapps.birthdaysremindme.utils.Constants;

import android.app.Application;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

public class MyApplication extends Application {

	public void onConfigurationChanged(Configuration conf) {
		setLocale();
	}

	public void onCreate() {
		setLocale();
	}

	private void setLocale() {
		String lang = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(Constants.PREFS_LANGUAGE, Constants.DEFAULT_LANG);
		Locale locale = new Locale(lang);
		Configuration newConfig = new Configuration();
		if (locale != null) {
			newConfig.locale = locale;
			Locale.setDefault(locale);
			getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
		}
	}
}
