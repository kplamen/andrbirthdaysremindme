package bg.androidapps.birthdaysremindme.comparators;

import java.util.Comparator;

import bg.androidapps.birthdaysremindme.db.BirthdayModel;

public class ComparatorByName implements Comparator<BirthdayModel>{

	@Override
	public int compare(BirthdayModel lhs, BirthdayModel rhs) {
		return lhs.getPerson().toLowerCase().compareTo(rhs.getPerson().toLowerCase());
	}

}
