package bg.androidapps.birthdaysremindme.comparators;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import bg.androidapps.birthdaysremindme.db.BirthdayModel;

public class ComparatorByDate implements Comparator<BirthdayModel> {

	@Override
	public int compare(BirthdayModel lhs, BirthdayModel rhs) {
		Calendar calLeft = Calendar.getInstance();
		Calendar calRight = Calendar.getInstance();
		Calendar calToday = Calendar.getInstance();
		
		calLeft.setTime(new Date(lhs.getBirthDate()));
		calRight.setTime(new Date(rhs.getBirthDate()));

		calLeft.set(Calendar.HOUR_OF_DAY, calToday.get(Calendar.HOUR_OF_DAY));
		calLeft.set(Calendar.MINUTE, calToday.get(Calendar.MINUTE));
		calLeft.set(Calendar.SECOND, calToday.get(Calendar.SECOND));
		calLeft.set(Calendar.MILLISECOND, calToday.get(Calendar.MILLISECOND));

		calRight.set(Calendar.HOUR_OF_DAY, calToday.get(Calendar.HOUR_OF_DAY));
		calRight.set(Calendar.MINUTE, calToday.get(Calendar.MINUTE));
		calRight.set(Calendar.SECOND, calToday.get(Calendar.SECOND));
		calRight.set(Calendar.MILLISECOND, calToday.get(Calendar.MILLISECOND));

		calLeft.add(Calendar.YEAR,
				calToday.get(Calendar.YEAR) - calLeft.get(Calendar.YEAR));

		calRight.add(Calendar.YEAR,
				calToday.get(Calendar.YEAR) - calRight.get(Calendar.YEAR));

		if (calLeft.compareTo(calToday) == 0
				|| calRight.compareTo(calToday) == 0) {
			if (calLeft.compareTo(calToday) == 0)
				return -1;
			if (calRight.compareTo(calToday) == 0)
				return 1;
			return 0;
		} else if (calLeft.after(calToday) && calRight.after(calToday)) {
			return calLeft.compareTo(calRight);
		} else if (calLeft.before(calToday) && calRight.before(calToday)) {
			return calLeft.compareTo(calRight);
		} else if (calLeft.before(calToday) && calRight.after(calToday)) {
			return 1;
		} else if (calLeft.after(calToday) && calRight.before(calToday)) {
			return -1;
		} else {
			return 0;
		}
	}

}
