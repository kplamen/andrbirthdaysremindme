package bg.androidapps.birthdaysremindme.db;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import bg.androidapps.birthdaysremindme.R;
import bg.androidapps.birthdaysremindme.io.CorruptedDataException;
import bg.androidapps.birthdaysremindme.utils.Util;
import bg.androidapps.birthdaysremindme.utils.ZodiacSign;

public class BirthdayDAO {

	private static BirthdayDAO self;

	private Context context;
	private SQLiteDatabase database;
	private SQLiteHelper dbHelper;
	private String[] allColumns = { SQLiteHelper.COLUMN_ID, SQLiteHelper.COLUMN_PERSON, SQLiteHelper.COLUMN_BIRTH_DATE, SQLiteHelper.COLUMN_REMINDME_WHEN,
			SQLiteHelper.COLUMN_IS_NOTIFIED, SQLiteHelper.COLUMN_UUID, SQLiteHelper.COLUMN_ZODIAC_ID };

	public static BirthdayDAO self(Context context) {
		if (self == null) {
			self = new BirthdayDAO(context);
		}
		
		return self;
	}

	private BirthdayDAO() {

	}

	private BirthdayDAO(Context pContext) {
		context = pContext;
		dbHelper = new SQLiteHelper(context);
	}

	public void open() throws SQLException {
		if(database == null || !database.isOpen())
			database = dbHelper.getWritableDatabase();
	}

	public void close() {
		if(database != null && database.isOpen())
			dbHelper.close();
	}

	public BirthdayModel createBirthday(String personName, long birthDate, int remindMeWhen, UUID uuid) throws ParseException, CorruptedDataException {
		if (remindMeWhen < 0 || remindMeWhen > 6)
			throw new CorruptedDataException(context.getString(R.string.corruptFileError));

		if (uuid == null)
			uuid = UUID.randomUUID();
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_PERSON, personName);
		values.put(SQLiteHelper.COLUMN_BIRTH_DATE, birthDate);
		values.put(SQLiteHelper.COLUMN_REMINDME_WHEN, remindMeWhen);
		values.put(SQLiteHelper.COLUMN_IS_NOTIFIED, 0);
		values.put(SQLiteHelper.COLUMN_UUID, uuid.toString());
		values.put(SQLiteHelper.COLUMN_ZODIAC_ID, Util.getZodiacId(birthDate).ordinal());

		long insertId = database.insert(SQLiteHelper.TABLE_BIRTHDAYS, null, values);
		Cursor cursor = database.query(SQLiteHelper.TABLE_BIRTHDAYS, allColumns, "id=" + insertId, null, null, null, null);
		cursor.moveToFirst();

		BirthdayModel newBirthday = cursorToBirthDay(cursor);

		cursor.close();

		return newBirthday;
	}

	public void editBirthday(long id, String personName, long birthDate, int remindMeWhen, ZodiacSign zodiacId) {
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_PERSON, personName);
		values.put(SQLiteHelper.COLUMN_BIRTH_DATE, birthDate);
		values.put(SQLiteHelper.COLUMN_REMINDME_WHEN, remindMeWhen);
		values.put(SQLiteHelper.COLUMN_IS_NOTIFIED, 0);
		values.put(SQLiteHelper.COLUMN_ZODIAC_ID, zodiacId.ordinal());
		database.update(SQLiteHelper.TABLE_BIRTHDAYS, values, "id=" + id, null);
	}

	public void deleteBirthday(BirthdayModel birthday) {
		long id = birthday.getId();
		database.delete(SQLiteHelper.TABLE_BIRTHDAYS, SQLiteHelper.COLUMN_ID + "=?", new String[] { String.valueOf(id) });
	}

	public void deleteAllBirthdays() {
		database.delete(SQLiteHelper.TABLE_BIRTHDAYS, null, null);
	}

	public List<BirthdayModel> getAllBirthdays() throws ParseException {
		List<BirthdayModel> birthDays = new ArrayList<BirthdayModel>();

		Cursor cursor = database.query(SQLiteHelper.TABLE_BIRTHDAYS, allColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			BirthdayModel birthDay = cursorToBirthDay(cursor);
			birthDays.add(birthDay);
			cursor.moveToNext();
		}

		cursor.close();

		return birthDays;
	}

	public ArrayList<BirthdayModel> getAllPendingBirthdays() throws ParseException {
		List<BirthdayModel> allBirthDays = getAllBirthdays();
		ArrayList<BirthdayModel> pendingBirthDays = new ArrayList<BirthdayModel>();

		Calendar todayDate = Calendar.getInstance();

		for (BirthdayModel b : allBirthDays) {
			Calendar remindDate = Calendar.getInstance();
			Calendar personDate = Calendar.getInstance();

				remindDate.set(Calendar.HOUR_OF_DAY, todayDate.get(Calendar.HOUR_OF_DAY));
				remindDate.set(Calendar.MINUTE, todayDate.get(Calendar.MINUTE));
				remindDate.set(Calendar.SECOND, todayDate.get(Calendar.SECOND));
				remindDate.set(Calendar.MILLISECOND, todayDate.get(Calendar.MILLISECOND));
				
				personDate.setTime(new Date(b.getBirthDate()));
				
				personDate.set(Calendar.HOUR_OF_DAY, todayDate.get(Calendar.HOUR_OF_DAY));
				personDate.set(Calendar.MINUTE, todayDate.get(Calendar.MINUTE));
				personDate.set(Calendar.SECOND, todayDate.get(Calendar.SECOND));
				personDate.set(Calendar.MILLISECOND, todayDate.get(Calendar.MILLISECOND));

			int remindMeWhen = b.getRemindMeWhen();
			remindDate.add(Calendar.DAY_OF_YEAR, remindMeWhen);

			personDate.add(Calendar.YEAR, todayDate.get(Calendar.YEAR) - personDate.get(Calendar.YEAR));

			if (personDate.compareTo(todayDate) == 0) {
				// today is birthday
				b.setDaysLeft(0);
				pendingBirthDays.add(b);
				continue;
			}

			personDate.add(Calendar.YEAR, remindDate.get(Calendar.YEAR) - personDate.get(Calendar.YEAR));

			if (personDate.compareTo(remindDate) == 0) {
				// today is for notification
				b.setDaysLeft(remindMeWhen);
				pendingBirthDays.add(b);
				continue;
			}

			for (int i = 1; i <= remindMeWhen; i++) {
				personDate.add(Calendar.DAY_OF_YEAR, -1);
				if (personDate.compareTo(remindDate) == 0) {
					break;
				}
				if (personDate.compareTo(todayDate) == 0) {
					b.setDaysLeft(i);
					pendingBirthDays.add(b);
					break;
				}
			}
		}

		return pendingBirthDays;
	}

	private BirthdayModel cursorToBirthDay(Cursor cursor) throws ParseException {
		BirthdayModel birthday = new BirthdayModel();
		birthday.setId(cursor.getLong(0));
		birthday.setPerson(cursor.getString(1));
		birthday.setBirthDate(cursor.getLong(2));
		birthday.setRemindMeWhen(cursor.getInt(3));
		birthday.setUuid(UUID.fromString(cursor.getString(5)));
		birthday.setZodiacId(ZodiacSign.values()[cursor.getInt(6)]);
		return birthday;
	}

}
