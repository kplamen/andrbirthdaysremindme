package bg.androidapps.birthdaysremindme.db;

import java.util.UUID;

import bg.androidapps.birthdaysremindme.utils.ZodiacSign;

import android.os.Parcel;
import android.os.Parcelable;

public class BirthdayModel implements Parcelable{
	
	private long id;
	private String person;
	private long birthDate;
	private String birthDateString;
	private int remindMeWhen;
	private boolean isNotified;
	private int daysLeft;
	private UUID uuid;
	private String timeUntilBd;
	private int timeUntilBdColor;
	private ZodiacSign zodiacId;
	
	public static final Parcelable.Creator<BirthdayModel> CREATOR = new Creator<BirthdayModel>() {
		
		@Override
		public BirthdayModel[] newArray(int size) {
			return new BirthdayModel[size];
		}
		
		@Override
		public BirthdayModel createFromParcel(Parcel source) {
			return new BirthdayModel(source);
		}
	};
	
	public BirthdayModel() {
		
	}
	
	public BirthdayModel(Parcel in) {
		readFromParcel(in);
	}
	
	private void readFromParcel(Parcel in) {
		id = in.readLong();
		person = in.readString();
		birthDate = in.readLong();
		daysLeft = in.readInt();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public long getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(long birthDate) {
		this.birthDate = birthDate;
	}

	public int getRemindMeWhen() {
		return remindMeWhen;
	}

	public void setRemindMeWhen(int remindMeWhen) {
		this.remindMeWhen = remindMeWhen;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(person);
		dest.writeLong(birthDate);
		dest.writeInt(daysLeft);
	}

	public boolean isNotified() {
		return isNotified;
	}

	public void setIsNotified(boolean isNotified) {
		this.isNotified = isNotified;
	}

	public int getDaysLeft() {
		return daysLeft;
	}

	public void setDaysLeft(int daysLeft) {
		this.daysLeft = daysLeft;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public ZodiacSign getZodiacId() {
		return zodiacId;
	}

	public void setZodiacId(ZodiacSign zodiacId) {
		this.zodiacId = zodiacId;
	}

	public String getTimeUntilBd() {
		return timeUntilBd;
	}

	public void setTimeUntilBd(String timeUntilBd) {
		this.timeUntilBd = timeUntilBd;
	}

	public int getTimeUntilBdColor() {
		return timeUntilBdColor;
	}

	public void setTimeUntilBdColor(int timeUntilBdColor) {
		this.timeUntilBdColor = timeUntilBdColor;
	}

	public String getBirthDateString() {
		return birthDateString;
	}

	public void setBirthDateString(String birthDateString) {
		this.birthDateString = birthDateString;
	}
	
}
