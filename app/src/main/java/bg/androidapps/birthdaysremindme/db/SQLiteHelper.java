package bg.androidapps.birthdaysremindme.db;

import java.text.ParseException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import bg.androidapps.birthdaysremindme.utils.Constants;
import bg.androidapps.birthdaysremindme.utils.ZodiacSign;
import bg.androidapps.birthdaysremindme.utils.Util;

public class SQLiteHelper extends SQLiteOpenHelper {
	public static final String TABLE_BIRTHDAYS = "birthdays";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_PERSON = "person";
	public static final String COLUMN_BIRTH_DATE = "birth_date";
	public static final String COLUMN_REMINDME_WHEN = "remindme_when";
	public static final String COLUMN_IS_NOTIFIED = "is_notified";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_ZODIAC_ID = "zodiac_id";

	private static final String DATABASE_NAME = "birthdays.db";
	private static final int DATABASE_VERSION = 3;

	private static final String DATABASE_CREATE = "create table " + TABLE_BIRTHDAYS + "(" + COLUMN_ID + " integer primary key autoincrement, " + COLUMN_PERSON
			+ " text not null," + COLUMN_BIRTH_DATE + " text," + COLUMN_REMINDME_WHEN + " integer," + COLUMN_IS_NOTIFIED + " integer," + COLUMN_UUID + " text,"
			+ COLUMN_ZODIAC_ID + " integer);";

	private static final String DB_UPGRADE_TO_2 = "alter table " + TABLE_BIRTHDAYS + " add column " + COLUMN_ZODIAC_ID + " integer;";
	
	public SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion == 1 && newVersion == 2) {
			fromOneToTwo(db);
		} else if(oldVersion == 1 && newVersion == DATABASE_VERSION) {
			fromOneToTwo(db);
			fromTwoToThree(db);
		} else if(oldVersion == 2 && newVersion == DATABASE_VERSION) {
			fromTwoToThree(db);
		}
	}
	
	private void fromOneToTwo(SQLiteDatabase db) {
		db.execSQL(DB_UPGRADE_TO_2);
		Cursor cursor = db.query(SQLiteHelper.TABLE_BIRTHDAYS, new String[] { COLUMN_ID, COLUMN_BIRTH_DATE }, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			try {
				long id = cursor.getLong(0);
				String date = cursor.getString(1);
				ZodiacSign zodiacId = Util.getZodiacId(date);
				ContentValues values = new ContentValues();
				values.put(COLUMN_ZODIAC_ID, zodiacId.ordinal());
				db.update(TABLE_BIRTHDAYS, values, "id=" + id, null);
				cursor.moveToNext();
			} catch (ParseException e) {
				// ignore it here, because the data comes from our old DB
			}
		}
	}
	
	private void fromTwoToThree(SQLiteDatabase db) {
		Cursor cursor = db.query(SQLiteHelper.TABLE_BIRTHDAYS, new String[] { COLUMN_ID, COLUMN_BIRTH_DATE }, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			try {
				long id = cursor.getLong(0);
				String date = cursor.getString(1);
				long dateAsLong = Constants.DEFAULT_DATE_FORMATTER.parse(date).getTime();
				
				ContentValues values = new ContentValues();
				values.put(COLUMN_BIRTH_DATE, dateAsLong);
				db.update(TABLE_BIRTHDAYS, values, "id=" + id, null);
				cursor.moveToNext();
			} catch (ParseException e) {
				// ignore it here, because the data comes from our old DB
			}
		}
	}
}
