package bg.androidapps.birthdaysremindme;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.util.LruCache;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import bg.androidapps.birthdaysremindme.comparators.ComparatorByDate;
import bg.androidapps.birthdaysremindme.comparators.ComparatorByName;
import bg.androidapps.birthdaysremindme.db.BirthdayDAO;
import bg.androidapps.birthdaysremindme.db.BirthdayModel;
import bg.androidapps.birthdaysremindme.io.BirthdaysExporter;
import bg.androidapps.birthdaysremindme.io.BirthdaysImporter;
import bg.androidapps.birthdaysremindme.io.CorruptedDataException;
import bg.androidapps.birthdaysremindme.menu.Settings;
import bg.androidapps.birthdaysremindme.utils.Constants;
import bg.androidapps.birthdaysremindme.utils.DateParseTask;
import bg.androidapps.birthdaysremindme.utils.Util;
import bg.androidapps.birthdaysremindme.utils.ZodiacSign;

public class MainActivity extends ListActivity {

	private boolean isActivityOnFront = false;

	private boolean isActionEdit = false;
	private int positionEditDelete = -1;

	private BirthdayDAO datasource;
	private ArrayAdapter<BirthdayModel> adapter;
	private List<BirthdayModel> adapterData;
	private List<BirthdayModel> allBirthdays;
	private static final int IMAGE_CACHE_SIZE = 3200000;
	private LruCache<Integer, Bitmap> imageCache;
	private PopupWindow popupWindow;

	private Comparator<BirthdayModel> comparator;
	private EditText search;

	private TextWatcher filterTextWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			adapter.getFilter().filter(s);
		}

	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		loadImages();

		this.getListView().setEmptyView(findViewById(R.id.list_empty_label));

		PreferenceManager.setDefaultValues(this, R.xml.prefs, false);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		if (prefs.getString(Constants.PREFS_SET_TIME, "missing").equals("missing"))
			prefs.edit().putString(Constants.PREFS_SET_TIME, Constants.DEFAULT_TIME_FOR_REMIND).commit();

		if (prefs.getInt(Constants.PREFS_SORT_BY, Constants.SORT_BY_DATE) == Constants.SORT_BY_DATE) {
			comparator = new ComparatorByDate();
		} else {
			comparator = new ComparatorByName();
		}

		datasource = BirthdayDAO.self(this);
		synchronized (datasource) {

			datasource.open();

			try {
				allBirthdays = datasource.getAllBirthdays();
			} catch (ParseException e) {
				toastMsg(this.getString(R.string.parsingDateError));
			}
			datasource.close();
		}
		adapterData = new ArrayList<BirthdayModel>(allBirthdays);

		imageCache = new LruCache<Integer, Bitmap>(IMAGE_CACHE_SIZE) {
			@Override
			protected int sizeOf(Integer key, Bitmap bitmap) {
				return bitmap.getRowBytes() * bitmap.getHeight();
			}
		};

		adapter = new MainAdapter(this, R.layout.listview_item_row, adapterData, allBirthdays, imageCache);
		setListAdapter(adapter);
		getListView().setLongClickable(true);
		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View v, int position, long id) {
				positionEditDelete = position;
				createDialog(Constants.DIALOG_DELETE);
				return true;
			}
		});

		search = (EditText) findViewById(R.id.searchInput);
		search.addTextChangedListener(filterTextWatcher);

		MyBroadcastReceiver.scheduleNextDay(getApplicationContext());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		for (int i = 0; i < 12; i++) {
			int resId = ZodiacSign.values()[i].getResId();
			Bitmap b = imageCache.get(Integer.valueOf(resId));
			if (b != null) {
				b.recycle();
				b = null;
			}
		}
		search.removeTextChangedListener(filterTextWatcher);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onResume() {
		super.onResume();
		setActivityOnFront(true);
		adapter.sort(comparator);
		adapter.notifyDataSetChanged();
		new DateParseTask(this).execute(allBirthdays);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (popupWindow != null) {
			popupWindow.dismiss();
			popupWindow = null;
		}
		setActivityOnFront(false);
		if (findViewById(R.id.searchInput).getVisibility() == View.VISIBLE)
			toggleSearch();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		BirthdayModel birthday = (BirthdayModel) getListView().getItemAtPosition(position);
		positionEditDelete = position;
		openAddEditDialog(birthday.getPerson(), birthday.getBirthDate(), birthday.getRemindMeWhen());
		isActionEdit = true;
	}

	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btnAdd:
				Calendar cal = Calendar.getInstance();
				long now = cal.getTime().getTime();
				openAddEditDialog("", now, Constants.DEFAULT_REMIND_ME_WHEN);
				break;
			case R.id.btnSearch:
				toggleSearch();
				break;
			case R.id.btnImport:
				importFromFile();
				break;
			case R.id.btnExport:
				exportToFile();
		}
	}

	public void toggleSearch() {
		if (search.getVisibility() == View.VISIBLE) {
			adapter.getFilter().filter(null);
			search.getText().clear();
			search.setVisibility(View.GONE);

			InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			in.hideSoftInputFromWindow(search.getWindowToken(), 0);

			findViewById(R.id.btnExport).setVisibility(View.VISIBLE);
			findViewById(R.id.btnImport).setVisibility(View.VISIBLE);
			((TextView) this.getListView().getEmptyView()).setText(R.string.emptyListLabel);
		} else {
			findViewById(R.id.btnExport).setVisibility(View.GONE);
			findViewById(R.id.btnImport).setVisibility(View.GONE);
			search.setVisibility(View.VISIBLE);
			search.requestFocus();

			InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			in.showSoftInput(search, 0);

			((TextView) this.getListView().getEmptyView()).setText(R.string.emptySearchLabel);
		}
	}

	public void sort(View view) {
		if (comparator instanceof ComparatorByName) {
			comparator = new ComparatorByDate();
			sort(Constants.SORT_BY_DATE);
		} else {
			comparator = new ComparatorByName();
			sort(Constants.SORT_BY_NAME);
		}
	}

	private void sort(int type) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		prefs.edit().putInt(Constants.PREFS_SORT_BY, type).commit();

		adapter.sort(comparator);
		adapter.notifyDataSetChanged();
	}

	protected void createDialog(final int type) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		switch (type) {
			case Constants.DIALOG_DELETE:
				builder.setMessage(getString(R.string.confirmDelete));
				break;
			case Constants.DIALOG_CLEAR:
				builder.setMessage(getString(R.string.confirmClear));
		}
		builder.setPositiveButton(R.string.btnDelete, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				synchronized (datasource) {
					datasource.open();
					if (type == Constants.DIALOG_DELETE) {
						BirthdayModel birthday = adapter.getItem(positionEditDelete);
						datasource.deleteBirthday(birthday);
						adapter.remove(birthday);
						allBirthdays.remove(birthday);
						positionEditDelete = -1;
					} else {
						datasource.deleteAllBirthdays();
						adapter.clear();
						allBirthdays.clear();
						if (findViewById(R.id.searchInput).getVisibility() == View.VISIBLE)
							toggleSearch();
					}
					datasource.close();
				}
				adapter.notifyDataSetChanged();
			}
		});
		builder.setNegativeButton(R.string.btnCancel, null);
		builder.create().show();
	}

	private void openAddEditDialog(String person, long dayMonth, int when) {
		Intent intent = new Intent(MainActivity.this, AddEditActivity.class);
		Bundle b = new Bundle();
		b.putString("person", person);
		b.putLong("date", dayMonth);
		b.putInt("when", when);
		intent.putExtras(b);

		startActivityForResult(intent, Constants.REQUEST_CODE_ADD_EDIT);
	}

	@Override
	protected void onActivityResult(int pRequestCode, int resultCode, Intent data) {
		if (pRequestCode == Constants.REQUEST_CODE_ADD_EDIT && resultCode == RESULT_OK) {
			Bundle b = data.getExtras();

			String person = b.getString("person");
			long dayMonth = b.getLong("date");
			int when = b.getInt("when");

			synchronized (datasource) {
				datasource.open();
				if (isActionEdit) {
					BirthdayModel birthday = (BirthdayModel) getListAdapter().getItem(positionEditDelete);
					ZodiacSign zodiacId = birthday.getZodiacId();

					try {
						zodiacId = Util.getZodiacId(dayMonth);
						birthday.setZodiacId(zodiacId);
					} catch (ParseException e) {
						// ignore, because the data comes from our activity
					}

					datasource.editBirthday(birthday.getId(), person, dayMonth, when, zodiacId);

					birthday.setPerson(person);
					birthday.setBirthDate(dayMonth);
					birthday.setRemindMeWhen(when);
					Util.setDateInfoToModel(birthday, birthday, getApplicationContext());

					allBirthdays.remove(positionEditDelete);
					allBirthdays.add(positionEditDelete, birthday);
					adapterData.remove(positionEditDelete);
					adapterData.add(positionEditDelete, birthday);
				} else {
					try {
						BirthdayModel birthday = datasource.createBirthday(person, dayMonth, when, null);
						Util.setDateInfoToModel(birthday, birthday, getApplicationContext());
						adapterData.add(birthday);
						allBirthdays.add(birthday);
					} catch (CorruptedDataException e) {
						// ignore here because the data is from our add/edit
						// activity
					} catch (ParseException e) {
						toastMsg(this.getString(R.string.parsingDateError));
					}
				}
				datasource.close();
			}

			adapter.notifyDataSetChanged();
		} else if (pRequestCode == Constants.REQUEST_CODE_SETTINGS && resultCode == RESULT_OK) {
			finish();
			Intent i = new Intent(getApplicationContext(), MainActivity.class);
			startActivity(i);
		}
		positionEditDelete = -1;
		isActionEdit = false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_settings:
				startActivityForResult(new Intent(this, Settings.class).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT), Constants.REQUEST_CODE_SETTINGS);
				return true;
			case R.id.menu_sort_by_name:
				comparator = new ComparatorByName();
				sort(Constants.SORT_BY_NAME);
				return true;
			case R.id.menu_sort_by_date:
				comparator = new ComparatorByDate();
				sort(Constants.SORT_BY_DATE);
				return true;
			case R.id.menu_export:
				exportToFile();
				return true;
			case R.id.menu_import:
				importFromFile();
				return true;
			case R.id.menu_clear:
				if (adapter.getCount() > 0)
					createDialog(Constants.DIALOG_CLEAR);
				return true;
			case R.id.menu_help:
				startActivity(new Intent(this, HelpActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public Comparator<BirthdayModel> getComparator() {
		return comparator;
	}

	public ArrayAdapter<BirthdayModel> getAdapter() {
		return this.adapter;
	}

	public List<BirthdayModel> getAllBirthdays() {
		return this.allBirthdays;
	}

	public List<BirthdayModel> getAdapterData() {
		return this.adapterData;
	}

	public boolean isActivityOnFront() {
		return isActivityOnFront;
	}

	public void setActivityOnFront(boolean isActivityOnFront) {
		this.isActivityOnFront = isActivityOnFront;
	}

	private void toastMsg(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}

	private void loadImages() {
		new AsyncTask<Void, Void, List<Bitmap>>() {

			@Override
			protected List<Bitmap> doInBackground(Void... params) {
				List<Bitmap> l = new ArrayList<Bitmap>(12);
				for (ZodiacSign s : ZodiacSign.values()) {
					Bitmap b = BitmapFactory.decodeResource(MainActivity.this.getResources(), s.getResId());
					l.add(b);
				}
				return l;
			}

			@Override
			protected void onPostExecute(List<Bitmap> result) {
				for (int i = 0; i < 12; i++) {
					ZodiacSign s = ZodiacSign.values()[i];
					imageCache.put(s.getResId(), result.get(i));
				}
			}

		}.execute();
	}

	@SuppressLint("InflateParams")
	public void onClickZodiac(View view) {
		LinearLayout anchor = (LinearLayout) view.getParent().getParent();
		String zodiacNumber = (String) view.getContentDescription();
		ZodiacSign zodiac = ZodiacSign.values()[Integer.parseInt(zodiacNumber)];

		LayoutInflater inflater = this.getLayoutInflater();
		View popupView = inflater.inflate(R.layout.popup, null);

		TextView popupTextView = (TextView) popupView.findViewById(R.id.popup_text);
		popupTextView.setText(getResources().getString(zodiac.getPopupId()));

		popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, true);

		Bitmap.Config conf = Bitmap.Config.ALPHA_8;
		Bitmap bmp = Bitmap.createBitmap(1, 1, conf);

		popupWindow.setBackgroundDrawable(new BitmapDrawable(getResources(), bmp));
		popupWindow.setOutsideTouchable(true);
		popupWindow.showAsDropDown(anchor, 0, -((ImageView) view).getHeight());

	}

	private void exportToFile() {
		if (adapter.getCount() > 0) {
			BirthdaysExporter exporter = new BirthdaysExporter(this);
			exporter.execute();
		} else {
			toastMsg(getResources().getString(R.string.noBirthdaysForExport));
		}
	}

	private void importFromFile() {
		BirthdaysImporter importer = new BirthdaysImporter(this);
		importer.execute();
	}

}
