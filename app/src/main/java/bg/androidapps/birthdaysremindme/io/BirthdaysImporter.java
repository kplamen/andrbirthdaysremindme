package bg.androidapps.birthdaysremindme.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;
import bg.androidapps.birthdaysremindme.MainActivity;
import bg.androidapps.birthdaysremindme.R;
import bg.androidapps.birthdaysremindme.db.BirthdayDAO;
import bg.androidapps.birthdaysremindme.db.BirthdayModel;
import bg.androidapps.birthdaysremindme.utils.Constants;
import bg.androidapps.birthdaysremindme.utils.Util;

public class BirthdaysImporter extends AsyncTask<Void, Void, Integer> {
	private Context context;
	private String message;
	private int importStatus = Constants.IO_STATUS_OK;

	private SimpleDateFormat formatter;

	public BirthdaysImporter(Context context) {
		this.context = context;
		this.formatter = new SimpleDateFormat(Constants.DEFAULT_DATE_FORMAT, Constants.LOCALE_DEFAULT);
	}

	@Override
	protected Integer doInBackground(Void... params) {
		List<String> result = (List<String>) readFromFile();

		int count = 0;
		if (result.size() > 3) {
			BirthdayDAO datasource = BirthdayDAO.self(this.context);
			synchronized (datasource) {
				datasource.open();
				try {
					outLbl: for (int i = 0; i < result.size(); i += 4) {
						String line = result.get(i);
						UUID uuid = UUID.fromString(line);
						for (BirthdayModel b : ((MainActivity) context).getAllBirthdays()) {
							if (b.getUuid().compareTo(uuid) == 0) {
								continue outLbl;
							}
						}
						BirthdayModel model = datasource.createBirthday(result.get(i + 1), getBirthDate(result.get(i + 2)),
								Integer.parseInt(result.get(i + 3)), uuid);
						count++;
						Util.setDateInfoToModel(model, model, context);
						model.setBirthDateString(Util.getBirthdateString(context, model));
						((MainActivity) context).getAdapterData().add(model);
						((MainActivity) context).getAllBirthdays().add(model);
					}
				} catch (Exception e) {
					importStatus = Constants.IO_STATUS_ERROR;
					message = context.getString(R.string.corruptFileError);
				} finally {
					datasource.close();
				}
			}
		} else {
			importStatus = Constants.IO_STATUS_ERROR;
			message = context.getString(R.string.corruptFileError);
		}
		return Integer.valueOf(count);
	}

	@Override
	protected void onPostExecute(Integer result) {
		if (importStatus == Constants.IO_STATUS_OK)
			message = result.intValue() + " "
					+ context.getString(R.string.birthdaysImported);

		((MainActivity) context).getAdapter().sort(
				((MainActivity) context).getComparator());
		((MainActivity) context).getAdapter().notifyDataSetChanged();

		if (((Activity) context).findViewById(R.id.searchInput).getVisibility() == View.VISIBLE)
			((MainActivity) context).toggleSearch();

		if (((MainActivity) context).isActivityOnFront()) {
			((Activity) context).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(context,
							BirthdaysImporter.this.getMessage(),
							Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	private List<String> readFromFile() {
		List<String> result = new ArrayList<String>();
		BufferedReader br = openStream();

		if (br != null) {
			try {
				String line;
				while ((line = br.readLine()) != null) {
					result.add(line);
				}
			} catch (Exception e) {
				importStatus = Constants.IO_STATUS_ERROR;
				message = context.getString(R.string.inputError);
			} finally {
				closeStream(br);
			}
		} else {
			importStatus = Constants.IO_STATUS_ERROR;
		}

		return result;
	}

	private BufferedReader openStream() {
		try {
			File fileOnSD = new File(Constants.EXT_STOR,
					Constants.DEFAULT_FILE_EXPORT);
			InputStream is = new FileInputStream(fileOnSD);
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			return br;
		} catch (FileNotFoundException e) {
			message = context.getString(R.string.fileNotFoundError);
			return null;
		}
	}

	private void closeStream(BufferedReader br) {
		try {
			if (br != null)
				br.close();
		} catch (IOException e) {
		}
	}

	String getMessage() {
		return message;
	}

	/**
	 * This method checks if stored date is in old format as formatted string
	 * instead of long value and return converted long value
	 * 
	 * @param line
	 *            the line containing the birth date
	 * @return the birth date as long in milliseconds
	 */
	private long getBirthDate(String line) throws ParseException {
		long result = 0l;
		try {
			result = Long.parseLong(line);
		} catch (NumberFormatException nfe) {
			result = formatter.parse(line).getTime();
		}

		return result;
	}
}
