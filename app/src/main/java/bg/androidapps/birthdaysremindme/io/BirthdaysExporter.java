package bg.androidapps.birthdaysremindme.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;
import bg.androidapps.birthdaysremindme.MainActivity;
import bg.androidapps.birthdaysremindme.R;
import bg.androidapps.birthdaysremindme.db.BirthdayDAO;
import bg.androidapps.birthdaysremindme.db.BirthdayModel;
import bg.androidapps.birthdaysremindme.utils.Constants;

public class BirthdaysExporter extends AsyncTask<Void, Void, Void> {
	private Context context;
	private String message;

	public BirthdaysExporter(Context context) {
		this.context = context;
	}

	@Override
	protected Void doInBackground(Void... params) {
		PrintWriter pw = openStream();

		if (pw != null) {
			BirthdayDAO datasource = BirthdayDAO.self(this.context);
			synchronized (datasource) {
				datasource.open();
				try {
					List<BirthdayModel> data = datasource.getAllBirthdays();

					for (BirthdayModel model : data) {
						pw.println(model.getUuid().toString());
						pw.println(model.getPerson());
						pw.println(model.getBirthDate());
						pw.println(String.valueOf(model.getRemindMeWhen()));
					}
					message = context.getString(R.string.birthdaysExported) + " " + Constants.EXT_STOR.getAbsolutePath() + File.separator
							+ Constants.DEFAULT_FILE_EXPORT;
				} catch (Exception e) {
					message = context.getString(R.string.outputError);
				} finally {
					closeStream(pw);
					datasource.close();
				}
			}
		}

		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		if (((Activity) context).findViewById(R.id.searchInput).getVisibility() == View.VISIBLE)
			((MainActivity) context).toggleSearch();
		if (((MainActivity) context).isActivityOnFront()) {
			((Activity) context).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(context, BirthdaysExporter.this.getMessage(), Toast.LENGTH_LONG).show();
				}
			});
		}
	}

	private PrintWriter openStream() {
		File destanationFile = new File(Constants.EXT_STOR, Constants.DEFAULT_FILE_EXPORT);

		if (!destanationFile.exists()) {
			try {
				destanationFile.createNewFile();
			} catch (Exception e) {
				message = context.getString(R.string.canNotCreateFileError);
				return null;
			}
		}

		try {
			FileWriter fw = new FileWriter(destanationFile, false);
			PrintWriter pw = new PrintWriter(fw, true);
			return pw;
		} catch (IOException e) {
			message = context.getString(R.string.outputError);
			return null;
		}

	}

	private void closeStream(PrintWriter pw) {
			if (pw != null)
				pw.close();
	}

	String getMessage() {
		return message;
	}
}
