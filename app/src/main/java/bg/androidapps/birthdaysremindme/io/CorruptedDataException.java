package bg.androidapps.birthdaysremindme.io;

public class CorruptedDataException extends RuntimeException{
	private static final long serialVersionUID = 3877509579469170968L;

	public CorruptedDataException(String message) {
		super(message);
	}
}
