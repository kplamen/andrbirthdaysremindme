package bg.androidapps.birthdaysremindme;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.text.Spanned;

import bg.androidapps.birthdaysremindme.db.BirthdayDAO;
import bg.androidapps.birthdaysremindme.db.BirthdayModel;
import bg.androidapps.birthdaysremindme.utils.Constants;

public class MyBroadcastReceiver extends BroadcastReceiver {
	
	PowerManager.WakeLock wakeLock;

	@Override
	public void onReceive(Context context, Intent intent) {
		
		if(intent.getAction().equals(Constants.REMIND_ACTION) || intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
			wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyBroadcastReceiver");
			wakeLock.acquire();
			checkAndNotify(context);
		}

		if(intent.getAction().equals(Constants.DISMISS_ACTION)) {
			dismissAlert(context);
		}
	}

	public static void scheduleNextDay(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String[] hourMin = prefs.getString(Constants.PREFS_SET_TIME, Constants.DEFAULT_TIME_FOR_REMIND).split(":");
		
		int sHour = Integer.parseInt(hourMin[0]);
		int sMin = Integer.parseInt(hourMin[1]);
		
		Calendar now = Calendar.getInstance();
		
		int hour = now.get(Calendar.HOUR_OF_DAY);
		int min = now.get(Calendar.MINUTE);
		
		if (sHour >= hour) {
			if (sHour == hour) {
				if (sMin > min) {
					now.set(Calendar.HOUR_OF_DAY, sHour);
					now.set(Calendar.MINUTE, sMin);
				} else {
					now.add(Calendar.DAY_OF_YEAR, 1);
					now.set(Calendar.HOUR_OF_DAY, sHour);
					now.set(Calendar.MINUTE, sMin);
				}
			} else {
				now.set(Calendar.HOUR_OF_DAY, sHour);
				now.set(Calendar.MINUTE, sMin);
			}
		} else {
			now.add(Calendar.DAY_OF_YEAR, 1);
			now.set(Calendar.HOUR_OF_DAY, sHour);
			now.set(Calendar.MINUTE, sMin);
		}

		long triggerAtTime = now.getTimeInMillis();

		Intent broadcastIntent = new Intent(context, MyBroadcastReceiver.class);
		broadcastIntent.setAction(Constants.REMIND_ACTION);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, broadcastIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC_WAKEUP, triggerAtTime, pendingIntent);
	}

	private void checkAndNotify(Context context) {
		CheckAndNotifyTask task = new CheckAndNotifyTask(context);
		task.execute();
	}

	private void dismissAlert(Context context) {
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(Constants.NOTIFICATION_ID);

		Intent broadcastIntent = new Intent(context, MyBroadcastReceiver.class);
		broadcastIntent.setAction(Constants.REMIND_ACTION);
		PendingIntent pending = PendingIntent.getBroadcast(context, 0, broadcastIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		pending.cancel();

		MyBroadcastReceiver.scheduleNextDay(context);
	}

	private class CheckAndNotifyTask extends AsyncTask<Void, Void, ArrayList<BirthdayModel>> {
		Context context;

		public CheckAndNotifyTask(Context context) {
			this.context = context;
		}

		@Override
		protected ArrayList<BirthdayModel> doInBackground(Void... params) {
			ArrayList<BirthdayModel> pendingBirthdays = new ArrayList<BirthdayModel>();
			BirthdayDAO datasource = BirthdayDAO.self(this.context);
			synchronized (datasource) {
				datasource.open();
				try {
					pendingBirthdays = datasource.getAllPendingBirthdays();
				} catch (ParseException e) {
					pendingBirthdays = null;
				}
				datasource.close();
			}
			return pendingBirthdays;
		}

		@Override
		protected void onPostExecute(ArrayList<BirthdayModel> pendingBirthdays) {
			String tickerText; 
			String notDesc;
			
			if(pendingBirthdays == null) {
				tickerText = context.getResources().getString(R.string.parsingDateError); 
				notDesc = context.getResources().getString(R.string.errorDuringCheck);
				notifyUser(pendingBirthdays, tickerText, notDesc);
			} else if (pendingBirthdays.size() > 0) {
				tickerText = context.getResources().getString(R.string.notificationText); 
				notDesc = context.getResources().getString(R.string.notificationDesc);
				notifyUser(pendingBirthdays, tickerText, notDesc);
			} else {
				MyBroadcastReceiver.scheduleNextDay(context);
				if(wakeLock != null)
					wakeLock.release();
			}
		}
		
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		private void notifyUser(ArrayList<BirthdayModel> pendingBirthdays, String tickerText, String notDesc) {
			int snooze = 0;
			
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
			snooze = Integer.parseInt(prefs.getString(Constants.PREFS_SNOOZE_INTERVAL, Constants.DEFAULT_SNOOZE_INTERVAL));
			
			Intent intent = new Intent(this.context, NotifyActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent. FLAG_ACTIVITY_CLEAR_TASK);
			intent.putParcelableArrayListExtra(Constants.KEY_PENDING_BIRTHDAYS, pendingBirthdays);
			PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

			NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
			inboxStyle.setBigContentTitle(tickerText);
			inboxStyle.setSummaryText(notDesc);

			ArrayList<Spanned> birthdays = NotifyActivity.prepareNotifications(context, pendingBirthdays);

			for (Spanned birthday : birthdays) {
				inboxStyle.addLine(birthday.toString());
			}

			NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext());
			builder.setStyle(inboxStyle);

			builder.setSmallIcon(R.drawable.birthday_icon);
			builder.setWhen(System.currentTimeMillis());
			builder.setContentTitle(tickerText);
			builder.setContentText(notDesc);
			builder.setDefaults(Notification.DEFAULT_VIBRATE|Notification.DEFAULT_SOUND);
			builder.setContentIntent(pendingIntent);

			boolean dismissOnSwipe = prefs.getBoolean(Constants.PREFS_DISMISS_ON_SWIPE, Constants.DEFAULT_DISMISS_ON_SWIPE);
			if(dismissOnSwipe) {
				Intent deleteIntent = new Intent(context, MyBroadcastReceiver.class);
				deleteIntent.setAction(Constants.DISMISS_ACTION);
				PendingIntent deletePendingIntent = PendingIntent.getBroadcast(context, 0, deleteIntent, 0);
				builder.setDeleteIntent(deletePendingIntent);
			}
			
			Notification notification = builder.build();
			
			NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(Constants.NOTIFICATION_ID, notification);

			if(pendingBirthdays == null || pendingBirthdays.size() == 0 || snooze == 0) {
				MyBroadcastReceiver.scheduleNextDay(context);
			} else {
				Intent broadcastIntent = new Intent(context, MyBroadcastReceiver.class);
				broadcastIntent.setAction(Constants.REMIND_ACTION);
				PendingIntent pending = PendingIntent.getBroadcast(context, 0, broadcastIntent, PendingIntent.FLAG_CANCEL_CURRENT);

				Calendar now = Calendar.getInstance();
				now.add(Calendar.MILLISECOND, snooze);
				long triggerAtTime = now.getTimeInMillis();

				AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
				alarmManager.set(AlarmManager.RTC_WAKEUP, triggerAtTime, pending);
			}
			
			if(wakeLock != null)
				wakeLock.release();
		}
	}

}
