package bg.androidapps.birthdaysremindme.utils;

import bg.androidapps.birthdaysremindme.R;

public enum ZodiacSign {
	CAPRICORN(R.drawable.capricorn, R.string.z_text_capricorn), AQUARIUS(R.drawable.aquarius, R.string.z_text_aquarius), PISCES(
			R.drawable.pisces, R.string.z_text_pisces), ARIES(R.drawable.aries, R.string.z_text_aries), TAURUS(
			R.drawable.taurus, R.string.z_text_taurus), GEMINI(R.drawable.gemini, R.string.z_text_gemini), CANCER(
			R.drawable.cancer, R.string.z_text_cancer), LEO(R.drawable.leo, R.string.z_text_leo), VIRGO(R.drawable.virgo, R.string.z_text_virgo), LIBRA(
			R.drawable.libra, R.string.z_text_libra), SCORPIO(R.drawable.scorpio, R.string.z_text_scorpio), SAGITTARIUS(
			R.drawable.sagittarius, R.string.z_text_sagittarius);
	
	ZodiacSign(int id, int popupId) {
		this.resourceId = id;
		this.popupId = popupId;
	}
	
	private int resourceId;
	private int popupId;
	
	public int getResId() {
		return resourceId;
	}
	
	public int getPopupId() {
		return popupId;
	}
}
