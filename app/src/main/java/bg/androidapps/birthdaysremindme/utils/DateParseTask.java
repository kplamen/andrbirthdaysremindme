package bg.androidapps.birthdaysremindme.utils;

import java.util.ArrayList;
import java.util.List;

import bg.androidapps.birthdaysremindme.MainActivity;
import bg.androidapps.birthdaysremindme.db.BirthdayModel;
import android.content.Context;
import android.os.AsyncTask;

public class DateParseTask extends AsyncTask<List<BirthdayModel>, String, List<BirthdayModel>>{
	private Context context;
	private List<BirthdayModel> birthdays;
	
	public DateParseTask(Context context) {
		this.context = context;
	}

	@Override
	protected List<BirthdayModel> doInBackground(List<BirthdayModel>... inputParams) {
		this.birthdays = inputParams[0];
		List<BirthdayModel> computedResults = new ArrayList<BirthdayModel>();
		for(BirthdayModel birthday: this.birthdays) {
			BirthdayModel currentModel = new BirthdayModel();
			Util.setDateInfoToModel(currentModel, birthday, context);
			computedResults.add(currentModel);
		}
		return computedResults;
	}
	
	protected void onPostExecute(List<BirthdayModel> computedResults) {
		for(int i = 0; i < birthdays.size(); i++) {
			BirthdayModel currentModel = birthdays.get(i);
			BirthdayModel currentResult = computedResults.get(i);
			currentModel.setTimeUntilBdColor(currentResult.getTimeUntilBdColor());
			currentModel.setTimeUntilBd(currentResult.getTimeUntilBd());
			currentModel.setBirthDateString(currentResult.getBirthDateString());
		}
		((MainActivity) context).getAdapter().notifyDataSetChanged();
	}
}
