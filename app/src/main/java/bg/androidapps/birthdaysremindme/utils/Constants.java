package bg.androidapps.birthdaysremindme.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.os.Environment;

public class Constants {
	
	public static final String REMIND_ACTION = "bg.androidapps.birthdaysremindme.REMIND_ACTION";
	public static final String DISMISS_ACTION = "bg.androidapps.birthdaysremindme.DISMISS_ACTION";
	public static final int NOTIFICATION_ID = 1000;
	public static final Locale LOCALE_DEFAULT = new Locale("en", "US");
	public static final Locale LOCALE_BG = new Locale("bg", "BG");
	public static final Locale LOCALE_FR = new Locale("fr", "FR");
	public static final String DEFAULT_DATE_FORMAT = "dd-MMMM-yyyy";
	public static final SimpleDateFormat DEFAULT_DATE_FORMATTER = new SimpleDateFormat("dd-MMMM-yyyy", LOCALE_DEFAULT);
	public static final String DEFAULT_LANG = "en";
	public static final String BG_LANG = "bg";
	public static final String FR_LANG = "fr";
	public static final String DEFAULT_TIME_FOR_REMIND = "13:00";
	public static final String DEFAULT_FILE_EXPORT = "birthdaysRemindMe.brr";
	public static final String DEFAULT_SNOOZE_INTERVAL = "300000";
	public static final boolean DEFAULT_DISMISS_ON_SWIPE = Boolean.FALSE.booleanValue();
	public static final File EXT_STOR = Environment.getExternalStorageDirectory();
	public static final String KEY_PENDING_BIRTHDAYS = "pendingBirthdays";
	
	public static final int DEFAULT_REMIND_ME_WHEN = 1;
	public static final int DIALOG_DELETE = 1;
	public static final int DIALOG_CLEAR = 2;
	public static final int SORT_BY_NAME = 0;
	public static final int SORT_BY_DATE = 1;
	
	public static final int IO_STATUS_OK = 0;
	public static final int IO_STATUS_ERROR = 1;
	
	public static final int REQUEST_CODE_ADD_EDIT = 0;
	public static final int REQUEST_CODE_SETTINGS = 1;
	
	public static final String PREFS_LANGUAGE = "lang";
	public static final String PREFS_SET_TIME = "setTime";
	public static final String PREFS_TIME_FORMAT = "timeFormat";
	public static final String PREFS_SORT_BY = "sortBy";
	public static final String PREFS_SNOOZE_INTERVAL = "snoozeInterval";
	public static final String PREFS_DISMISS_ON_SWIPE = "dismissOnSwipe";


	private Constants() {
	}

}
