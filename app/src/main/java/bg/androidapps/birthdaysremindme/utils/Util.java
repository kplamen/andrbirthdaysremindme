package bg.androidapps.birthdaysremindme.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import bg.androidapps.birthdaysremindme.R;
import bg.androidapps.birthdaysremindme.db.BirthdayModel;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Util {

	private Util() {
	}

	public static ZodiacSign getZodiacId(long birthdate) throws ParseException {
		int[] dateAsInts = getDayMonthAsInts(birthdate);
		int day = dateAsInts[0];
		int month = dateAsInts[1];

		switch (month) {
		case 0:
			if(day < 21)
				return ZodiacSign.CAPRICORN;
			else
				return ZodiacSign.AQUARIUS;
		case 1:
			if(day < 20)
				return ZodiacSign.AQUARIUS;
			else
				return ZodiacSign.PISCES;
		case 2:
			if(day < 21)
				return ZodiacSign.PISCES;
			else
				return ZodiacSign.ARIES;
		case 3:
			if(day < 21)
				return ZodiacSign.ARIES;
			else
				return ZodiacSign.TAURUS;
		case 4:
			if(day < 22)
				return ZodiacSign.TAURUS;
			else
				return ZodiacSign.GEMINI;
		case 5:
			if(day < 22)
				return ZodiacSign.GEMINI;
			else
				return ZodiacSign.CANCER;
		case 6:
			if(day < 23)
				return ZodiacSign.CANCER;
			else
				return ZodiacSign.LEO;
		case 7:
			if(day < 23)
				return ZodiacSign.LEO;
			else
				return ZodiacSign.VIRGO;
		case 8:
			if(day < 24)
				return ZodiacSign.VIRGO;
			else
				return ZodiacSign.LIBRA;
		case 9:
			if(day < 24)
				return ZodiacSign.LIBRA;
			else
				return ZodiacSign.SCORPIO;
		case 10:
			if(day < 23)
				return ZodiacSign.SCORPIO;
			else
				return ZodiacSign.SAGITTARIUS;
		case 11:
			if(day < 22)
				return ZodiacSign.SAGITTARIUS;
			else
				return ZodiacSign.CAPRICORN;
			default:
				return null;
		}
	}

	private static int[] getDayMonthAsInts(long date) throws ParseException {
		Calendar calendar = Calendar.getInstance();

		int[] result = new int[2];
		calendar.setTime(new Date(date));
		result[0] = calendar.get(Calendar.DAY_OF_MONTH);
		result[1] = calendar.get(Calendar.MONTH);

		return result;
	}
	
	public static String getRemainingTimeColor(Context context, int timeLeft) {
		int result;
		
		if (timeLeft == 0) {
			result = context.getResources().getColor(R.color.red);
		} else if (timeLeft < 8) {
			result = context.getResources().getColor(R.color.orange);
		} else {
			result = context.getResources().getColor(R.color.green);
		}

		return String.valueOf(result);
	}
	
	public static String getBirthdateString(Context context,
			BirthdayModel originalModel) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		
		SimpleDateFormat formatter = new SimpleDateFormat(
				Constants.DEFAULT_DATE_FORMAT, Util.getLocaleByLang(prefs
						.getString(Constants.PREFS_LANGUAGE,
								Constants.DEFAULT_LANG)));
		
		String dateString = formatter.format(new Date(originalModel.getBirthDate()));
		return dateString;
	}
	
	public static void setDateInfoToModel(BirthdayModel model, BirthdayModel originalModel, Context context) {
		Calendar todayDate = Calendar.getInstance();
		Calendar nextBirthdayDate = Calendar.getInstance();

		nextBirthdayDate.setTime(new Date(originalModel.getBirthDate()));

		nextBirthdayDate.set(Calendar.HOUR_OF_DAY, todayDate.get(Calendar.HOUR_OF_DAY));
		nextBirthdayDate.set(Calendar.MINUTE, todayDate.get(Calendar.MINUTE));
		nextBirthdayDate.set(Calendar.SECOND, todayDate.get(Calendar.SECOND));
		nextBirthdayDate.set(Calendar.MILLISECOND, todayDate.get(Calendar.MILLISECOND));

		nextBirthdayDate.add(Calendar.YEAR, todayDate.get(Calendar.YEAR) - nextBirthdayDate.get(Calendar.YEAR));

		if (nextBirthdayDate.before(todayDate))
			nextBirthdayDate.add(Calendar.YEAR, 1);

		int daysLeft = 0;

		while (todayDate.compareTo(nextBirthdayDate) != 0) {
			todayDate.add(Calendar.DAY_OF_YEAR, 1);
			daysLeft++;
		}
		
		String remainingTimeColor = Util.getRemainingTimeColor(context, daysLeft);
		String timeLeftStr = Util.getTimeLeftAsString(context, daysLeft, nextBirthdayDate);
		String birthdateStr = Util.getBirthdateString(context, originalModel);

		model.setTimeUntilBdColor(Integer.parseInt(remainingTimeColor));
		model.setTimeUntilBd(timeLeftStr);
		model.setBirthDateString(birthdateStr);
	}
	
	private static String getTimeLeftAsString(Context context, int timeLeft, Calendar nextBirthdayDate) {
		StringBuilder result = new StringBuilder();

		if (timeLeft > 0) {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(context.getApplicationContext());
			String lang = prefs.getString(Constants.PREFS_LANGUAGE,
					Constants.DEFAULT_LANG);
			
			if (lang.equals(Constants.DEFAULT_LANG)) {
				result.append(timeLeft);
				result.append(" ");
				result.append(timeLeft == 1 ? context.getResources().getString(R.string.day) : context.getResources().getString(R.string.days));
				result.append(" ");
				result.append(timeLeft == 1 ? context.getResources().getString(R.string.left) : context.getResources().getString(R.string.leftPlural));
				if(timeLeft < 8) {
					result.append(" - ");
					result.append(getDayOfWeek(nextBirthdayDate, context));
				}
			} else {
				result.append(timeLeft == 1 ? context.getResources().getString(R.string.left) : context.getResources().getString(R.string.leftPlural));
				result.append(" ");
				result.append(timeLeft);
				result.append(" ");
				result.append(timeLeft == 1 ? context.getResources().getString(R.string.day) : context.getResources().getString(R.string.days));
				if(timeLeft < 8) {
					result.append(" - ");
					result.append(getDayOfWeek(nextBirthdayDate, context));
				}
			}
		} else {
			result.append(context.getResources().getString(R.string.hasBirthday));
		}

		return result.toString();
	}
	
	private static String getDayOfWeek(Calendar nextBirthdayDate, Context context) {
		int dayOfWeek = nextBirthdayDate.get(Calendar.DAY_OF_WEEK);
		switch (dayOfWeek) {
		case Calendar.MONDAY:
			return context.getResources().getString(R.string.monday);
		case Calendar.TUESDAY:
			return context.getResources().getString(R.string.tuesday);
		case Calendar.WEDNESDAY:
			return context.getResources().getString(R.string.wednesday);
		case Calendar.THURSDAY:
			return context.getResources().getString(R.string.thursday);
		case Calendar.FRIDAY:
			return context.getResources().getString(R.string.friday);
		case Calendar.SATURDAY:
			return context.getResources().getString(R.string.saturday);
		case Calendar.SUNDAY:
			return context.getResources().getString(R.string.sunday);
		default:
			return "";
		}
	}
	
	public static Locale getLocaleByLang(String lang) {
		if(lang.equals(Constants.DEFAULT_LANG)) {
			return Constants.LOCALE_DEFAULT;
		} else if(lang.equals(Constants.BG_LANG)) {
			return Constants.LOCALE_BG;
		} else if(lang.equals(Constants.FR_LANG)) {
			return Constants.LOCALE_FR;
		}
		
		return null;
	}
	
	public static ZodiacSign getZodiacId(String birthdate) throws ParseException {
		long dateAsLong = Constants.DEFAULT_DATE_FORMATTER.parse(birthdate).getTime();
		return getZodiacId(dateAsLong);
	}
}
