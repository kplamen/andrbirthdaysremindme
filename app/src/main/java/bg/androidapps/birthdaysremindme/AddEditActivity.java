package bg.androidapps.birthdaysremindme;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import bg.androidapps.birthdaysremindme.utils.Constants;
import bg.androidapps.birthdaysremindme.utils.Util;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AddEditActivity extends Activity {

	private Calendar cal = Calendar.getInstance();
	private Bundle b;
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, monthOfYear);
			cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			SimpleDateFormat formatter = new SimpleDateFormat(
					Constants.DEFAULT_DATE_FORMAT, Util.getLocaleByLang(prefs
							.getString(Constants.PREFS_LANGUAGE,
									Constants.DEFAULT_LANG)));
			String date = formatter.format(cal.getTime());
			((Button) AddEditActivity.this.findViewById(R.id.dayMonth))
					.setText(date);
		}
	};

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_add_edit);

		setTitle(R.string.addEditTitle);

		b = getIntent().getExtras();

		long date = b.getLong("date");
		((EditText) findViewById(R.id.person)).setText(b.getString("person"));
		((Spinner) findViewById(R.id.when)).setSelection(b.getInt("when"));
		cal.setTime(new Date(date));
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		SimpleDateFormat formatter = new SimpleDateFormat(
				Constants.DEFAULT_DATE_FORMAT, Util.getLocaleByLang(prefs
						.getString(Constants.PREFS_LANGUAGE,
								Constants.DEFAULT_LANG)));
		String dateAsString = formatter.format(cal.getTime());
		((Button) findViewById(R.id.dayMonth)).setText(dateAsString);
	}

	public void onClick(View view) {
		boolean isWrongDataEntered = false;
		switch (view.getId()) {
		case R.id.btnOk:
			String person = ((EditText) findViewById(R.id.person)).getText().toString();
			if(person == null || person.equals("")) {
				Toast.makeText(this, R.string.errorWrongData, Toast.LENGTH_SHORT).show();
				isWrongDataEntered = true;
			}
			int when = (int) ((Spinner) findViewById(R.id.when)).getSelectedItemId();
			long date = cal.getTime().getTime();
			Bundle b = new Bundle();
			b.putString("person", person);
			b.putLong("date", date);
			b.putInt("when", when);
			getIntent().putExtras(b);
			setResult(RESULT_OK, getIntent());
			break;
		default:
			setResult(RESULT_CANCELED);
		}

		if(!isWrongDataEntered)
			finish();
	}

	public void setDayMonth(View v) {
		MyDatePickerDialog picker = new MyDatePickerDialog(cal, this, mDateSetListener);
		picker.show();
	}
}
