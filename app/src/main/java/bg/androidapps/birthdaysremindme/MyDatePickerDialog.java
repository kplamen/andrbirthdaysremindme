package bg.androidapps.birthdaysremindme;

import java.lang.reflect.Field;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.DatePicker;

public class MyDatePickerDialog extends DatePickerDialog {

	public MyDatePickerDialog(Calendar dateToShow, Context context, OnDateSetListener callBack) {
		super(context, null, dateToShow.get(Calendar.YEAR), dateToShow.get(Calendar.MONTH), dateToShow.get(Calendar.DAY_OF_MONTH));
		initializePicker(callBack);
	}

	private void initializePicker(final OnDateSetListener callback) {
		try {
			// If you're only using Honeycomb+ then you can just call
			// getDatePicker() instead of using reflection
			Field pickerField = DatePickerDialog.class.getDeclaredField("mDatePicker");
			pickerField.setAccessible(true);
			final DatePicker picker = (DatePicker) pickerField.get(this);
			this.setCancelable(true);
			this.setButton(DialogInterface.BUTTON_NEGATIVE, getContext().getResources().getString(R.string.btnCancel), (OnClickListener) null);
			this.setButton(DialogInterface.BUTTON_POSITIVE, getContext().getResources().getString(R.string.btnOk), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					picker.clearFocus(); // Focus must be cleared so the value
											// change listener is called
					callback.onDateSet(picker, picker.getYear(), picker.getMonth(), picker.getDayOfMonth());
				}
			});
		} catch (Exception e) { /* Reflection probably failed */
		}
	}
}
