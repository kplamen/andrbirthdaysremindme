package bg.androidapps.birthdaysremindme;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import bg.androidapps.birthdaysremindme.db.BirthdayModel;

public class MainAdapter extends ArrayAdapter<BirthdayModel> {
	private Context context;
	private List<BirthdayModel> allBirthdays;
	private List<BirthdayModel> adapterData;
	private Filter mFilter;
	private LruCache<Integer, Bitmap> imageCache;

	public MainAdapter(Context context, int customViewResourceId, List<BirthdayModel> adapterData, List<BirthdayModel> allValues,
			LruCache<Integer, Bitmap> imageCache) {
		super(context, customViewResourceId, adapterData);
		this.context = context;
		this.allBirthdays = allValues;
		this.adapterData = adapterData;
		this.imageCache = imageCache;
	}

	@Override
	public int getCount() {
		return this.adapterData.size();
	}

	@Override
	public BirthdayModel getItem(int position) {
		return this.adapterData.get(position);
	}

	@Override
	public void clear() {
		adapterData.clear();
	}

	@Override
	public void remove(BirthdayModel birthday) {
		adapterData.remove(birthday);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		CustomListRow customListRow = null;
		BirthdayModel birthday = adapterData.get(position);

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.listview_item_row, null);
			customListRow = new CustomListRow();
			row.setTag(customListRow);
		} else {
			customListRow = (CustomListRow) row.getTag();
		}

		customListRow.firstRow = (TextView) row.findViewById(R.id.first_row);
		customListRow.secondRow = (TextView) row.findViewById(R.id.second_row);
		customListRow.thirdRow = (TextView) row.findViewById(R.id.third_row);
		customListRow.fourthRow = (TextView) row.findViewById(R.id.fourth_row);
		customListRow.zodiacImage = (ImageView) row
				.findViewById(R.id.zodiac_image);

		String[] whenValues = context.getResources().getStringArray(
				R.array.when);
		String when = whenValues[birthday.getRemindMeWhen()];

		customListRow.firstRow.setText(birthday.getPerson());
		customListRow.secondRow.setText(birthday.getBirthDateString());
		customListRow.thirdRow.setText(context.getString(R.string.notify) + " "
				+ when);

		customListRow.fourthRow.setTextColor(birthday.getTimeUntilBdColor());

		customListRow.fourthRow.setText(birthday.getTimeUntilBd());

		int zodiacImageId = birthday.getZodiacId().getResId();
		Bitmap zodiacIcon = null;
		if (imageCache.get(Integer.valueOf(zodiacImageId)) != null) {
			zodiacIcon = imageCache.get(Integer.valueOf(zodiacImageId));
		} else {
			zodiacIcon = BitmapFactory.decodeResource(context.getResources(),
					zodiacImageId);
			imageCache.put(Integer.valueOf(zodiacImageId), zodiacIcon);
		}
		customListRow.zodiacImage.setImageBitmap(zodiacIcon);
		customListRow.zodiacImage.setContentDescription(String
				.valueOf(birthday.getZodiacId().ordinal()));

		return row;
	}

	@Override
	public Filter getFilter() {
		if (mFilter == null) {
			mFilter = new ItemsFilter();
		}
		return mFilter;
	}

	private class ItemsFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence prefix) {
			FilterResults results = new FilterResults();
			adapterData.clear();

			if (prefix == null || prefix.length() == 0) {
				adapterData.addAll(allBirthdays);
				results.values = adapterData;
				results.count = adapterData.size();
			} else {
				for (BirthdayModel model : allBirthdays) {
					if (model.getPerson().toLowerCase()
							.startsWith(prefix.toString().toLowerCase())
							|| model.getBirthDateString()
									.toLowerCase()
									.startsWith(prefix.toString().toLowerCase())) {
						adapterData.add(model);
					}
				}

				results.values = adapterData;
				results.count = adapterData.size();
			}

			return results;
		}

		@Override
		protected void publishResults(CharSequence prefix, FilterResults results) {
			MainAdapter.this.sort(((MainActivity) context).getComparator());
			notifyDataSetChanged();
		}
	}

	static class CustomListRow {
		TextView firstRow;
		TextView secondRow;
		TextView thirdRow;
		TextView fourthRow;
		ImageView zodiacImage;
	}
}
