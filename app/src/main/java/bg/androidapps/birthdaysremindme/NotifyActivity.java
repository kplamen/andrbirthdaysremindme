package bg.androidapps.birthdaysremindme;

import java.util.ArrayList;
import java.util.List;

import bg.androidapps.birthdaysremindme.db.BirthdayModel;
import bg.androidapps.birthdaysremindme.utils.Constants;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

public class NotifyActivity extends ListActivity {
	// TODO make tests with API 10 and xlarge display
	
	private long snooze;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notify);
		setTitle(R.string.notifyActivityTitle);
		
		this.getListView().setEmptyView(findViewById(R.id.list_empty_label));
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		List<BirthdayModel> pendingBirthdays = getIntent().getExtras().getParcelableArrayList(Constants.KEY_PENDING_BIRTHDAYS);
		if(pendingBirthdays == null) {
			hideDoNotDismissBtn();
			ArrayAdapter<String> emptyAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, new ArrayList<String>(0));
			setListAdapter(emptyAdapter);
			return;
		}
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		snooze = Long.parseLong(prefs.getString(Constants.PREFS_SNOOZE_INTERVAL, Constants.DEFAULT_SNOOZE_INTERVAL));
		
		if(snooze == 0) {
			hideDoNotDismissBtn();
		}
		
		ArrayList<Spanned> notifications = prepareNotifications(getApplicationContext(), pendingBirthdays);
		ArrayAdapter<Spanned> adapter = new ArrayAdapter<Spanned>(this, android.R.layout.simple_list_item_1, notifications);
		
		setListAdapter(adapter);
	}
	
	private void hideDoNotDismissBtn() {
		findViewById(R.id.btnDoNotDismiss).setVisibility(View.GONE);
		((Button)findViewById(R.id.btnDismiss)).setText(R.string.btnClose);
	}
	
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnDismiss:
			if(snooze == 0) {
				this.dismissAlert();
			} else {
				createDialog();
			}
			break;
		case R.id.btnDoNotDismiss:
			this.finish();
		}
	}
	
	public static ArrayList<Spanned> prepareNotifications(Context context, List<BirthdayModel> pendingBirthdays) {
		ArrayList<Spanned> result = new ArrayList<Spanned>(pendingBirthdays.size());

		for (BirthdayModel b : pendingBirthdays) {
			if (b.getDaysLeft() == 0) {
				result.add(Html.fromHtml("<b>" + b.getPerson() + "</b>" + " "
						+ context.getString(R.string.hasBirthday)));
			} else {
				SharedPreferences prefs = PreferenceManager
						.getDefaultSharedPreferences(context);
				String lang = prefs.getString(Constants.PREFS_LANGUAGE,
						Constants.DEFAULT_LANG);
				boolean isOneDayLeft = b.getDaysLeft() == 1;
				if (lang.equals(Constants.DEFAULT_LANG)) {
					result.add(Html.fromHtml("<b>"
							+ b.getPerson()
							+ "</b>"
							+ " - "
							+ b.getDaysLeft()
							+ (isOneDayLeft ? " " + context.getString(R.string.day)
									+ " " : " " + context.getString(R.string.days)
									+ " ")
							+ (isOneDayLeft ? " " + context.getString(R.string.left)
									: " " + context.getString(R.string.leftPlural))));
				} else {
					result.add(Html.fromHtml("<b>"
							+ b.getPerson()
							+ "</b>"
							+ " - "
							+ (isOneDayLeft ? " " + context.getString(R.string.left)
									+ " " : " "
									+ context.getString(R.string.leftPlural) + " ")
							+ b.getDaysLeft()
							+ (isOneDayLeft ? " " + context.getString(R.string.day)
									: " " + context.getString(R.string.days))));
				}
			}
		}

		return result;
	}
	
	private void dismissAlert() {
		NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(Constants.NOTIFICATION_ID);
		
		Intent broadcastIntent = new Intent(getApplicationContext(), MyBroadcastReceiver.class);
		broadcastIntent.setAction(Constants.REMIND_ACTION);
		PendingIntent pending = PendingIntent.getBroadcast(getApplicationContext(), 0, broadcastIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		pending.cancel();
		
		MyBroadcastReceiver.scheduleNextDay(getApplicationContext());
		NotifyActivity.this.finish();
	}
	
	private void createDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.confirmDismiss));
		
		builder.setPositiveButton(R.string.btnDismiss, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				NotifyActivity.this.dismissAlert();
			}
		});
		builder.setNegativeButton(R.string.btnCancel, null);
		builder.create().show();
	}
}
