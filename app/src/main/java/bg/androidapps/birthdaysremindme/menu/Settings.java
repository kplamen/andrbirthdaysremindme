package bg.androidapps.birthdaysremindme.menu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import bg.androidapps.birthdaysremindme.MyBroadcastReceiver;
import bg.androidapps.birthdaysremindme.R;
import bg.androidapps.birthdaysremindme.utils.Constants;
import bg.androidapps.birthdaysremindme.utils.Util;

public class Settings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

	private String currentLang;
	private SettingsFragment prefsFragment;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTitle(R.string.settingsTitle);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			addPreferencesFromResource(R.xml.prefs);

			getListView().setCacheColorHint(Color.TRANSPARENT);
			getListView().setBackgroundColor(getResources().getColor(R.color.background_blue));
			setTimeForRemindTitle();
		} else {
			prefsFragment = new SettingsFragment();
			getFragmentManager().beginTransaction().replace(android.R.id.content, prefsFragment).commit();
		}
		PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		currentLang = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(Constants.PREFS_LANGUAGE, Constants.DEFAULT_LANG);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(Constants.PREFS_TIME_FORMAT)) {
			setTimeForRemindTitle();
		} else if (key.equals(Constants.PREFS_SET_TIME)) {
			setTimeForRemindTitle();
			MyBroadcastReceiver.scheduleNextDay(getApplicationContext());
		} else if (key.equals(Constants.PREFS_LANGUAGE)) {
			String newLang = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(Constants.PREFS_LANGUAGE, Constants.DEFAULT_LANG);
			if (!currentLang.equals(newLang)) {
				this.currentLang = newLang;
				Locale locale = new Locale(newLang);
				Locale.setDefault(locale);
				Configuration config = getBaseContext().getResources().getConfiguration();
				config.locale = locale;
				getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
				setResult(RESULT_OK);
				finish();
			} else {
				setResult(RESULT_CANCELED);
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressWarnings("deprecation")
	protected void setTimeForRemindTitle() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String time = prefs.getString(Constants.PREFS_SET_TIME, Constants.DEFAULT_TIME_FOR_REMIND);
		boolean is24HourView = prefs.getBoolean(Constants.PREFS_TIME_FORMAT, false);
		String lang = prefs.getString(Constants.PREFS_LANGUAGE, Constants.DEFAULT_LANG);

		SimpleDateFormat sdf = null;
		if (is24HourView) {
			sdf = new SimpleDateFormat("HH:mm", Util.getLocaleByLang(lang));
		} else {
			sdf = new SimpleDateFormat("KK:mm", Util.getLocaleByLang(lang));
		}
		Date date = null;
		try {
			date = sdf.parse(time);
		} catch (ParseException e) {
			this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(Settings.this, R.string.parsingTimeError, Toast.LENGTH_LONG).show();
				}
			});
		}
		if (is24HourView) {
			sdf = new SimpleDateFormat("HH:mm", Util.getLocaleByLang(lang));
		} else {
			sdf = new SimpleDateFormat("hh:mm aa", Util.getLocaleByLang(lang));
		}
		Preference timePref;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			timePref = (TimePreference) findPreference(Constants.PREFS_SET_TIME);
		} else {
			timePref = prefsFragment.findPreference(Constants.PREFS_SET_TIME);
		}
		
		timePref.setTitle(sdf.format(date));
	}

	@Override
	public void onStop() {
		super.onStop();
		if (!isFinishing())
			finish();
	}
}
