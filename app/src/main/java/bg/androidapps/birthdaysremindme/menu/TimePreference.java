package bg.androidapps.birthdaysremindme.menu;


import bg.androidapps.birthdaysremindme.R;
import bg.androidapps.birthdaysremindme.utils.Constants;
import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

public class TimePreference extends DialogPreference {
	private int lastHour = 0;
    private int lastMinute = 0;
	private TimePicker picker;

	public TimePreference(Context ctxt) {
		this(ctxt, null);
	}

	public TimePreference(Context ctxt, AttributeSet attrs) {
		this(ctxt, attrs, 0);
	}

	public TimePreference(Context ctxt, AttributeSet attrs, int defStyle) {
		super(ctxt, attrs, defStyle);

		setPositiveButtonText(getContext().getResources().getString(R.string.btnTimePickerSet));
		setNegativeButtonText(getContext().getResources().getString(R.string.btnTimePickerCancel));
	}

	@Override
	protected View onCreateDialogView() {
		picker = new TimePicker(getContext());
		return (picker);
	}
	
	@Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);
        picker.setIs24HourView(PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(Constants.PREFS_TIME_FORMAT, false));
        picker.setCurrentHour(lastHour);
        picker.setCurrentMinute(lastMinute);
    }
	
	@Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return(a.getString(index));
    }
	
	@Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
		String[] time = getPersistedString(Constants.DEFAULT_TIME_FOR_REMIND).split(":");
		
        lastHour = Integer.parseInt(time[0]);
        lastMinute = Integer.parseInt(time[1]);
    }

	@Override
	protected void onDialogClosed(boolean positiveResult) {

		if (positiveResult) {
			picker.clearFocus();
			lastHour = picker.getCurrentHour();
			lastMinute = picker.getCurrentMinute();

			String time = String.valueOf(lastHour) + ":" + String.valueOf(lastMinute);

			if (callChangeListener(time)) {
                persistString(time);
			}
		}
		super.onDialogClosed(positiveResult);
	}

}
